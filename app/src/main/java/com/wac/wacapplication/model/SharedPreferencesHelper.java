package com.wac.wacapplication.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import java.util.Locale;

public class SharedPreferencesHelper {
    private static final String FILE_NAME = "file_lang"; // preference file name
    private static final String KEY_LANG = "key_lang"; // preference key
    public final SharedPreferences mPref;

    Context context;
    public SharedPreferencesHelper(Context context) {
        this.context = context;
        mPref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public void saveLanguage(String lang) {
        // we can use this method to save language
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(KEY_LANG, lang);
        editor.apply();
        // we have saved
        // recreate activity after saving to load the new language, this is the same
        // as refreshing activity to load new language
        //recreate();
    }
    public void loadLanguage() {
        // we can use this method to load language,
        // this method should be called before setContentView() method of the onCreate method
        Locale locale = new Locale(getLangCode());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
    public  String getLangCode() {
        String langCode = mPref.getString(KEY_LANG, "ar");
        // save english 'en' as the default language
        return langCode;
    }

    public   boolean isArabic() {
        if (getLangCode().equals("ar")) return true;
        return false;
    }
}
