package com.wac.wacapplication.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class ResponseProg {

	@SerializedName("msg")
	private String msg;

	@SerializedName("http_code")
	private int httpCode;

	@SerializedName("data")
	private List<DataItemProg> data;

	@SerializedName("status")
	private boolean status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setHttpCode(int httpCode){
		this.httpCode = httpCode;
	}

	public int getHttpCode(){
		return httpCode;
	}

	public void setDataProd(List<DataItemProg> data){
		this.data = data;
	}

	public List<DataItemProg> getDataProg(){
		return data;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseProg{" +
			"msg = '" + msg + '\'' + 
			",http_code = '" + httpCode + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}