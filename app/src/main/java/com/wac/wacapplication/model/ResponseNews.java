package com.wac.wacapplication.model;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

//@Generated("com.robohorse.robopojogenerator")
public class ResponseNews {

	@SerializedName("msg")
	private String msg;

	@SerializedName("http_code")
	private int httpCode;

	@SerializedName("data")
	private ArrayList<DataItemNews> data;

	@SerializedName("status")
	private boolean status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setHttpCode(int httpCode){
		this.httpCode = httpCode;
	}

	public int getHttpCode(){
		return httpCode;
	}

	public void setData(ArrayList<DataItemNews> data){
		this.data = data;
	}

	public ArrayList<DataItemNews> getData(){
		return data;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseNews{" +
			"msg = '" + msg + '\'' + 
			",http_code = '" + httpCode + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}