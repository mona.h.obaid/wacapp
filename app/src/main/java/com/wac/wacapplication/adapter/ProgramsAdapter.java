package com.wac.wacapplication.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wac.wacapplication.R;
import com.wac.wacapplication.model.DataItemProg;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProgramsAdapter extends RecyclerView.Adapter<ProgramsAdapter.ProgramsHolder> implements View.OnClickListener{
    ArrayList<DataItemProg> dataProg;
    Activity activity;
    ArrayList<String> urlImages=new ArrayList<>();

    public ProgramsAdapter(ArrayList<DataItemProg> dataProg, Activity activity) {
        this.dataProg = dataProg;
        this.activity = activity;
    }
    @NonNull
    @Override
    public ProgramsAdapter.ProgramsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(activity).inflate(R.layout.programes_item,null,false);
        return new ProgramsHolder(root);
    }
    public void setProgramesList(ArrayList<DataItemProg> data){
        this.dataProg=data;
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(@NonNull ProgramsAdapter.ProgramsHolder holder, final int position) {
        urlImages.clear();
        urlImages= (ArrayList<String>) extractUrls(dataProg.get(position).getPostContent());
        if(urlImages.size()>0){

            Glide.with(activity).load(urlImages.get(0)).fitCenter().into(holder.imgProgrames);
        }
        else
            holder.imgProgrames.setImageResource(R.drawable.wacnewprog2);
            holder.txtTitle.setText(dataProg.get(position).getPostTitle());
            holder.txtDate.setText(dataProg.get(position).getPostDate());
            holder.shareProg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = dataProg.get(position).getPostTitle()+"\n"+dataProg.get(position).getGuid();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        holder.txtDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(v.getContext(), DetailsNewsActivity.class);
//                intent.putStringArrayListExtra("urlImage",urlImages);
//                activity.startActivity(intent);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(dataProg.get(position).getGuid()));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataProg.size();
    }
    @Override
    public void onClick(View view) {
    }
    public class ProgramsHolder extends RecyclerView.ViewHolder {
        public ImageView imgProgrames;
        public TextView txtTitle;
        public TextView txtDate;
        public TextView txtDetails;
        public ImageButton shareProg;
        public ProgramsHolder(@NonNull View itemView) {
            super(itemView);
            imgProgrames = itemView.findViewById(R.id.img_program);
            txtTitle = itemView.findViewById(R.id.txt_proTitle);
            txtDate = itemView.findViewById(R.id.txt_proDate);
            txtDetails = itemView.findViewById(R.id.txt_proDetail);
            shareProg = itemView.findViewById(R.id.share_program);
        }
    }
    public static List<String> extractUrls(String text)
    {
        List<String> containedUrls = new ArrayList<String>();
        //String urlRegex = "((https?):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        String urlRegex =  "(http(s?):/)(/[^/]+)+" + "\\.(?:jpg)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);
        while (urlMatcher.find())
        {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }
        return containedUrls;
    }
}
