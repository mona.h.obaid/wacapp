package com.wac.wacapplication.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wac.wacapplication.R;
import com.wac.wacapplication.model.DataItemAdver;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdverAdapter extends RecyclerView.Adapter<AdverAdapter.AdverHolder> implements View.OnClickListener , Filterable {
    ArrayList<DataItemAdver> data;
    ArrayList<DataItemAdver> mOriginalValues  ;
    Activity activity;
    ArrayList<String> urlImages =new ArrayList<>();
    private InputFilter filter;



    public AdverAdapter(ArrayList<DataItemAdver> data, Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public AdverAdapter.AdverHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(activity).inflate(R.layout.adver_item,null,false);

        return new AdverAdapter.AdverHolder(root);
    }

    public void setAdverList(ArrayList<DataItemAdver> data){
        this.data=data;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull final AdverAdapter.AdverHolder holder, final int position) {

        String[] words = data.get(position).getPostTitle().split(" ");
        String title="";

        if(words.length>7) {
            for (int i = 0; i <= 7; i++) {
                title = title +" "+words[i];

            }
            holder.txtTitleAdver.setText(title+"...");
        }
        else
            holder.txtTitleAdver.setText(data.get(position).getPostTitle()+"...");


        holder.txtDateAdver.setText(data.get(position).getPostDate());

        holder.shareAdver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = data.get(position).getPostTitle()+"\n"+data.get(position).getGuid();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));


            }
        });

        holder.txtDetailsAdver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(data.get(position).getGuid()));
                activity.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                data = (ArrayList<DataItemAdver>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<DataItemAdver> FilteredArrList = new ArrayList<DataItemAdver>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<DataItemAdver>(data); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        String data = mOriginalValues.get(i).getPostTitle();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(new DataItemAdver(mOriginalValues.get(i).getPostTitle(),mOriginalValues.get(i).getPostDate(),mOriginalValues.get(i).getGuid()));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public class AdverHolder extends RecyclerView.ViewHolder {
        public ImageView imgAdver;
        public TextView txtTitleAdver;
        public TextView txtDateAdver;
        public TextView txtDetailsAdver;
        public ImageButton shareAdver;


        public AdverHolder(@NonNull View itemView) {
            super(itemView);
            imgAdver = itemView.findViewById(R.id.img_adver);
            txtTitleAdver = itemView.findViewById(R.id.txt_adverTitle);
            txtDateAdver = itemView.findViewById(R.id.txt_adverDate);
            txtDetailsAdver = itemView.findViewById(R.id.txt_adverDetail);
            shareAdver = itemView.findViewById(R.id.share_adver);
        }
    }
    public static List<String> extractUrls(String text)
    {
        List<String> containedUrls = new ArrayList<String>();
        //String urlRegex = "((https?):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        String urlRegex =  "(http(s?):/)(/[^/]+)+" + "\\.(?:jpg)";

        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find())
        {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }



}
