package com.wac.wacapplication.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.wac.wacapplication.R;
import com.wac.wacapplication.model.DataItemNews;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewsAdapter extends RecyclerView .Adapter<NewsAdapter.NewsHolder> implements View.OnClickListener {
    ArrayList<DataItemNews> data;

    Activity activity;
    ArrayList<String> urlImages=new ArrayList<>();


    public NewsAdapter(ArrayList<DataItemNews> data, Activity activity) {
        this.data = data;
        this.activity = activity;

    }

    @NonNull
    @Override
    public NewsAdapter.NewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(activity).inflate(R.layout.news_item,null,false);

        return new NewsHolder(root);
    }

    public void setNewsList(ArrayList<DataItemNews> data){
        this.data=data;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.NewsHolder holder, final int position) {
        urlImages.clear();
        urlImages= (ArrayList<String>) extractUrls(data.get(position).getPostContent());
      if(urlImages.size()>0){
       Glide.with(activity).load(urlImages.get(0)).into(holder.imgNews);

      }
      else
          holder.imgNews.setImageResource(R.drawable.wacnewprog2);

          holder.txtTitle.setText(data.get(position).getPostTitle());
          holder.txtDate.setText(data.get(position).getPostDate());

        holder.shareNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = data.get(position).getPostTitle()+"\n"+data.get(position).getGuid();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));


            }
        });

        holder.txtDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(data.get(position).getGuid()));
                activity.startActivity(intent);

//
//                Intent intent = new Intent(v.getContext(), DetailsNewsActivity.class);
//                intent.putStringArrayListExtra("urlImage",urlImages);
//                activity.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onClick(View v) {

    }

    public class NewsHolder extends RecyclerView.ViewHolder {
        public ImageView imgNews;
        public TextView txtTitle;
        public TextView txtDate;
        public TextView txtDetails;
        public ImageButton shareNews;


        public NewsHolder(@NonNull View itemView) {
            super(itemView);
            imgNews = itemView.findViewById(R.id.img_new);
            txtTitle = itemView.findViewById(R.id.txt_newTitle);
            txtDate = itemView.findViewById(R.id.txt_newDate);
            txtDetails = itemView.findViewById(R.id.txt_newDetail);
            shareNews = itemView.findViewById(R.id.share_new);
        }
    }
    public static List<String> extractUrls(String text)
    {
        List<String> containedUrls = new ArrayList<String>();
        //String urlRegex = "((https?):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        String urlRegex =  "(http(s?):/)(/[^/]+)+" + "\\.(?:jpg)";

        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find())
        {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }
}
