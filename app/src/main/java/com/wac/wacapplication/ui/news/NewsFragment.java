package com.wac.wacapplication.ui.news;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.wac.wacapplication.R;
import com.wac.wacapplication.adapter.NewsAdapter;
import com.wac.wacapplication.model.DataItemNews;
import com.wac.wacapplication.model.SharedPreferencesHelper;

import java.util.ArrayList;

public class NewsFragment extends Fragment {

    private NewsViewModel newsViewModel;
    private ArrayList<DataItemNews> data;
    private NewsAdapter newsAdapter;
    private RecyclerView recyclerViewNews;
    private  SwipeRefreshLayout mySwipeRefreshLayout;
    public static Context context;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context=getContext();
        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_news, container, false);

        mySwipeRefreshLayout = root.findViewById(R.id.mySwipeRefreshLayout);
        mySwipeRefreshLayout.setRefreshing(true);


        data=new ArrayList<>();
        recyclerViewNews = root.findViewById(R.id.rv_news);
        RecyclerView.LayoutManager managerGrid=new GridLayoutManager(getContext(),1);
        recyclerViewNews.setLayoutManager(managerGrid);
        newsAdapter=new NewsAdapter(data,getActivity());
        recyclerViewNews.setAdapter(newsAdapter);


        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            mySwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {

                                newsViewModel.refreshNews();
                        }
                    }
            );

            fetchNews();

        } else {
            Toast.makeText(getContext(),"Chech internet connection", Toast.LENGTH_SHORT).show();

        }


        return root;
    }

    private void fetchNews() {
        mySwipeRefreshLayout.setRefreshing(true);
        newsViewModel.getDataNews().observe(this, new Observer<ArrayList<DataItemNews>>() {
            @Override
            public void onChanged(ArrayList<DataItemNews> dataItemNews) {
                displayNews(dataItemNews);
            }
        });
    }

    public void displayNews(ArrayList<DataItemNews> data){
        mySwipeRefreshLayout.setRefreshing(false);
        newsAdapter.setNewsList(data);
    }


}