package com.wac.wacapplication.ui.news;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.wac.wacapplication.model.DataItemNews;
import com.wac.wacapplication.model.ResponseNews;
import com.wac.wacapplication.model.SharedPreferencesHelper;
import com.wac.wacapplication.utils.ApiClient;
import com.wac.wacapplication.utils.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsViewModel extends ViewModel {

    private MutableLiveData<ArrayList<DataItemNews>> mData;
    private SharedPreferencesHelper sharedPreferencesHelper;

    public NewsViewModel() {
        mData = new MutableLiveData<>();
        sharedPreferencesHelper=new SharedPreferencesHelper(NewsFragment.context);
        refreshNews();
        }

      //--------------------------------- get data from api by retrofit ----------------------------
       public void refreshNews( ){

          //------------------------------- get data ar ---------------------------------------------
        if(sharedPreferencesHelper.getLangCode().equals("ar")){
           ApiInterface apiInterface=ApiClient.getClient().create(ApiInterface.class);
               Call<ResponseNews> responseNewsCall=apiInterface.getData();
               responseNewsCall.enqueue(new Callback<ResponseNews>() {
                   @Override
                   public void onResponse(Call<ResponseNews> call, Response<ResponseNews> response) {
                       int statusCode=response.code();

                       ArrayList <DataItemNews> data=response.body().getData();
                       mData.setValue(data);
                   }

                   @Override
                   public void onFailure(Call<ResponseNews> call, Throwable t) {
                   }
               });}
          else
              refreshNewsEn();
           }

    public void refreshNewsEn()
    { ApiInterface apiInterface=ApiClient.getClient().create(ApiInterface.class);


        Call<ResponseNews> responseNewsCall=apiInterface.getDataNewEn();
        responseNewsCall.enqueue(new Callback<ResponseNews>() {
            @Override
            public void onResponse(Call<ResponseNews> call, Response<ResponseNews> response) {
                int statusCode=response.code();
                ArrayList <DataItemNews> data=response.body().getData();
                mData.setValue(data);
            }

            @Override
            public void onFailure(Call<ResponseNews> call, Throwable t) {

            }
        });}


    public LiveData<ArrayList<DataItemNews>> getDataNews() {
        return mData;
    }
}