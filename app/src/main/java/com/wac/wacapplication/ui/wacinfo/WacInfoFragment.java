package com.wac.wacapplication.ui.wacinfo;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.wac.wacapplication.R;

public class WacInfoFragment extends Fragment {

    private WacInfoViewModel wacInfoViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        wacInfoViewModel =
                ViewModelProviders.of(this).get(WacInfoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_wac_info, container, false);
        //final TextView textView = root.findViewById(R.id.text_slideshow);
        wacInfoViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

    //            textView.setText(s);
            }
        });
        return root;
    }


}