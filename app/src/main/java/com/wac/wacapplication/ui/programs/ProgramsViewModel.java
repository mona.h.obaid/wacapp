package com.wac.wacapplication.ui.programs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.wac.wacapplication.model.DataItemProg;
import com.wac.wacapplication.model.ResponseProg;
import com.wac.wacapplication.model.SharedPreferencesHelper;
import com.wac.wacapplication.utils.ApiClient;
import com.wac.wacapplication.utils.ApiInterface;

import java.util.ArrayList;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProgramsViewModel extends ViewModel {
    private MutableLiveData<ArrayList<DataItemProg>> mText;
   // Context context;
    SharedPreferencesHelper sharedPreferencesHelper ;

    public ProgramsViewModel() {

        mText = new MutableLiveData<>();
        sharedPreferencesHelper=new SharedPreferencesHelper(ProgramesFragment.context);
        refreshPrograms();
    }


    //--------------------------------- get data from api by retrofit ----------------------------

    public void refreshPrograms() {
        String lang= sharedPreferencesHelper.getLangCode();
        if(lang.equals("ar")){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseProg> responseProgArCall = apiService.getDataProg();
        responseProgArCall.enqueue(new Callback<ResponseProg>() {
            @Override
            public void onResponse(Call<ResponseProg> call, Response<ResponseProg> response) {
                int statusCode = response.code();
                ArrayList<DataItemProg> dataItemProgs = (ArrayList<DataItemProg>) response.body().getDataProg();
                mText.setValue(dataItemProgs);
            }

            @Override
            public void onFailure(Call<ResponseProg> call, Throwable t) {

            }
        });}

        else
            refreshProgramsEn();

    }

    public void refreshProgramsEn() {
        mText = new MutableLiveData<>();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseProg> responseProgArCall = apiService.getDataProgEn();
        responseProgArCall.enqueue(new Callback<ResponseProg>() {
            @Override
            public void onResponse(Call<ResponseProg> call, Response<ResponseProg> response) {
                int statusCode = response.code();
                ArrayList<DataItemProg> dataItemProgs = (ArrayList<DataItemProg>) response.body().getDataProg();
                mText.setValue(dataItemProgs);
            }

            @Override
            public void onFailure(Call<ResponseProg> call, Throwable t) {

            }
        });

    }

    public LiveData<ArrayList<DataItemProg>> getText() {
        return mText;

    }



//    public void setLang(String lang)
//    {
//        this.lang=lang;
//    }
}