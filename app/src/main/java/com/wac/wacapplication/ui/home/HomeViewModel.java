package com.wac.wacapplication.ui.home;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.wac.wacapplication.model.DataItemAdver;
import com.wac.wacapplication.model.ResponseAdver;
import com.wac.wacapplication.utils.ApiClient;
import com.wac.wacapplication.utils.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<ArrayList<DataItemAdver>> mData;

    public HomeViewModel() {
        mData = new MutableLiveData<>();
        refreshAdver();

    }

    public void refreshAdver() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseAdver> responseAdverArCall = apiService.getDataAdverAr();
        responseAdverArCall.enqueue(new Callback<ResponseAdver>() {
            @Override
            public void onResponse(Call<ResponseAdver> call, Response<ResponseAdver> response) {
                int statusCode = response.code();
                // mText.setValue(response.body().getData().ge;
                ArrayList<DataItemAdver> data = (ArrayList<DataItemAdver>) response.body().getDataAdverAr();
                mData.setValue(data);
            }

            @Override
            public void onFailure(Call<ResponseAdver> call, Throwable t) {
                Log.e("wac", "Error");

            }

        });
    }
        public LiveData<ArrayList<DataItemAdver>> getDataAdver() {
            return mData;
        }
}