package com.wac.wacapplication.ui.contactus;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wac.wacapplication.R;

public class ContactUsFragment extends Fragment {

    private ContactUsModel contactUsModel;
    private SupportMapFragment mapFragment;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        contactUsModel =
                ViewModelProviders.of(this).get(ContactUsModel.class);
        View root = inflater.inflate(R.layout.fragment_contact_us, container, false);
      //  final TextView textView = root.findViewById(R.id.text_tools);

        final ImageView imgYoutube=root.findViewById(R.id.img_youtube);
        final ImageView imgFacebook=root.findViewById(R.id.img_facebook);
        final ImageView imgInstgrame=root.findViewById(R.id.img_instgram);
        final ImageView imgTiwtter=root.findViewById(R.id.img_twitter);
        final TextView  txtWeb=root.findViewById(R.id.txt_web);
        final TextView  txtTel1=root.findViewById(R.id.txt_tel1);
        final TextView  txtTel2=root.findViewById(R.id.txt_tel2);


        contactUsModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
       //         textView.setText(s);
            }
        });

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    LatLng latLng = new LatLng(31.528520, 34.454175);
                    googleMap.addMarker(new MarkerOptions().position(latLng).title("wac"));
                    //   googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,12));
                    googleMap.getUiSettings().setMapToolbarEnabled(true);
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    googleMap.getUiSettings().setMapToolbarEnabled(true);
                }
            });
        }

        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();

        //------------------  Youtube ----------------------

        imgYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.youtube.com/user/wacgaza"));
                startActivity(intent);
            }
        });

        //------------------  Facebook ----------------------

        imgFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.facebook.com/wacps"));
                startActivity(intent);
            }
        });

        //------------------  Twitter ----------------------

        imgTiwtter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://twitter.com/Wac_Gaza"));
                startActivity(intent);
            }
        });

        //------------------  Instgrame ----------------------

        imgInstgrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.instagram.com/wacgaza/"));
                startActivity(intent);
            }
        });

        //----------------------- Web Site ---------------------
        txtWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://site.wac.ps/ar/"));
                startActivity(intent);


            }
        });

        txtTel1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+txtTel1.getText().toString()));
                startActivity(intent);
                return true;
            }
        });

        txtTel2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+txtTel2.getText().toString()));
                startActivity(intent);
                return true;
            }
        });



        return root;
    }
//    @Override
//    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//        menu.removeItem(android.R.id.cut);
//
//    }



}