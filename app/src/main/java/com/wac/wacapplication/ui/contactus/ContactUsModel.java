package com.wac.wacapplication.ui.contactus;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ContactUsModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ContactUsModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is contact us  fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}