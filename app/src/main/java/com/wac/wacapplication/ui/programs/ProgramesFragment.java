package com.wac.wacapplication.ui.programs;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.wac.wacapplication.R;
import com.wac.wacapplication.adapter.ProgramsAdapter;
import com.wac.wacapplication.model.DataItemProg;
import com.wac.wacapplication.model.SharedPreferencesHelper;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProgramesFragment extends Fragment {
  private ProgramsViewModel programsViewModel;
    private ArrayList<DataItemProg> data;
    private ProgramsAdapter programsAdapter;
    private RecyclerView recyclerViewProgrames;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private SharedPreferencesHelper sharedPreferencesHelper;

    public static Context context ;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context=getContext();
        programsViewModel= ViewModelProviders.of(this).get(ProgramsViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_programs, container, false);
        mySwipeRefreshLayout = root.findViewById(R.id.mySwipeRefreshLayout);
        mySwipeRefreshLayout.setRefreshing(true);
        data=new ArrayList<>();
        recyclerViewProgrames = root.findViewById(R.id.rv_programs);
        RecyclerView.LayoutManager managerGrid=new GridLayoutManager(getContext(),1);
        recyclerViewProgrames.setLayoutManager(managerGrid);
        programsAdapter=new ProgramsAdapter(data,getActivity());
        recyclerViewProgrames.setAdapter(programsAdapter);
;

        mySwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
          @Override
          public void onRefresh() {

                  programsViewModel.refreshPrograms();


          }
      });

        fetchProgrames();
        return root;
    }
    private void fetchProgrames() {
        mySwipeRefreshLayout.setRefreshing(true);
        programsViewModel.getText().observe(this, new Observer<ArrayList<DataItemProg>>() {
            @Override
            public void onChanged(ArrayList<DataItemProg> data) {
                displayProgrames(data);
            }
        });
    }
    public void displayProgrames(ArrayList<DataItemProg> data){
        mySwipeRefreshLayout.setRefreshing(false);
        programsAdapter.setProgramesList(data);
    }



}