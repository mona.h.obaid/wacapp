package com.wac.wacapplication.ui.home;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.wac.wacapplication.R;
import com.wac.wacapplication.adapter.AdverAdapter;
import com.wac.wacapplication.model.DataItemAdver;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private  ArrayList<DataItemAdver> data;
    private AdverAdapter adverAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private HomeViewModel homeViewModel;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        mySwipeRefreshLayout = root.findViewById(R.id.mySwipeRefreshLayout);
        mySwipeRefreshLayout.setRefreshing(true);

        final TextView txtTitle=root.findViewById(R.id.txt_adverTitle);
        data = new ArrayList<>();
        recyclerView = root.findViewById(R.id.rv_adver);
        RecyclerView.LayoutManager managerGrid = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(managerGrid);
        adverAdapter = new AdverAdapter(data, getActivity());
        recyclerView.setAdapter(adverAdapter);

        final EditText txtSearch =root.findViewById(R.id.txtSearch);


        // check internet connection -------------------

        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            mySwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {

                            homeViewModel.refreshAdver();
                        }
                    }
            );

            fetchAdver();
        } else {
            Toast.makeText(getContext(),"Chech internet connection", Toast.LENGTH_SHORT).show();

        }

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                adverAdapter.getFilter().filter(s.toString());
            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adverAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {
                adverAdapter.getFilter().filter(s);
            }
        });

        return root;


    }


    private void fetchAdver() {
        mySwipeRefreshLayout.setRefreshing(true);

        homeViewModel.getDataAdver().observe(this, new Observer<ArrayList<DataItemAdver>>() {
            @Override
            public void onChanged(ArrayList<DataItemAdver> dataItemAdvers) {
                displayAdver(dataItemAdvers);
            }
        });
    }

    public void displayAdver(ArrayList<DataItemAdver> data) {
        mySwipeRefreshLayout.setRefreshing(false);
        adverAdapter.setAdverList(data);
    }





}