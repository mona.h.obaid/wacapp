package com.wac.wacapplication.ui.wacinfo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class WacInfoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public WacInfoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is wac information fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}