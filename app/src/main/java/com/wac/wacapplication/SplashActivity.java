package com.wac.wacapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.wac.wacapplication.model.SharedPreferencesHelper;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity {
    private int splashTime=2500;
    private  Handler myHndler;
    private  Locale myLocale;
    SharedPreferencesHelper sharedPreferencesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferencesHelper = new SharedPreferencesHelper(this);
        sharedPreferencesHelper.loadLanguage();
       // loadLocale();

        ImageView imgWac=findViewById(R.id.image_wac);
        Animation animationImg= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink);

        imgWac.startAnimation(animationImg);

        myHndler=new Handler();
        myHndler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        },splashTime);

    }


    //---------------------------- Language ---------------------------
    public void loadLocale() {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",0);
        String language = prefs.getString(langPref, "");
        changeLang(language);
    }

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
        {
            saveLocale(myLocale.getLanguage());
            return;
        }
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

    }

    // -------------------------  save lang in  SharedPreferences---------
    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }
}
