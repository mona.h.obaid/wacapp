package com.wac.wacapplication.utils;//package com.h.alrekhawi.jretrofitproductsmanager.utils;

// import android.support.v4.util.ArrayMap;

import androidx.annotation.NonNull;

//import com.h.alrekhawi.jretrofitproductsmanager.model.ProductResponse;
//import org.jetbrains.annotations.*;

import com.wac.wacapplication.model.ResponseAdver;
import com.wac.wacapplication.model.ResponseNews;
import com.wac.wacapplication.model.ResponseProg;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
 // -------------------------------- API News AR -------------------------
    @GET("ar/api/?cat_id=17")
    @NonNull
    Call<ResponseNews> getData();

 // -------------------------------- API Advertisements AR --------------
    @GET("ar/api/?cat_id=38")
    @NonNull
  Call<ResponseAdver> getDataAdverAr();

 // -------------------------------- API Programs AR --------------------
    @GET("ar/api/?cat_id=24")
    @NonNull
  Call<ResponseProg> getDataProg();

// -------------------------------- API News EN -------------------------
    @GET("en/api/?cat_id=2")
    @NonNull
  Call<ResponseNews> getDataNewEn();

// -------------------------------- API Advertisements EN ---------------
//    @GET("?cat_id=56")
//    @NonNull
//  Call<ResponseAdverEn> getDataAdvEn();

 //-------------------------------- API Programs EN ---------------------
    @GET("en/api/?cat_id=3")
    @NonNull
  Call<ResponseProg> getDataProgEn();



//
//    @FormUrlEncoded
//    @POST("addProduct")
//    @NonNull
 //   Call<TaskResponse> addProduct(@FieldMap @NonNull ArrayMap<String, String> var1);

//    @FormUrlEncoded
//    @POST("addProduct")
//    Call<ProductResponse> addProductFields(@Field("p_name") String title,
//                                           @Field("p_descrption") String description,
//                                           @Field("p_price") String price,
//                                           @Field("p_offer_price") String offerPrice,
//                                           @Field("p_image") String image);
//
//    @FormUrlEncoded
//    @POST("deleteProduct")
//    @NotNull
//    Call<ProductResponse> deleteProduct(@FieldMap @NotNull ArrayMap<String, String> var1);
}
