package com.wac.wacapplication.utils;//package com.h.alrekhawi.jretrofitproductsmanager.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {
    //http://www.beststrends.com/taskshazemdisable/api/index.php/getProducts
    public static final String BASE_URL = "https://site.wac.ps/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
