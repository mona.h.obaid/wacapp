package com.wac.wacapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.wac.wacapplication.model.DataItemNews;
import com.wac.wacapplication.model.SharedPreferencesHelper;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Locale myLocale;
    Dialog dialog;
    ArrayList<DataItemNews> data;
    NiftyDialogBuilder niftyDialogBuilder;
    private AppBarConfiguration mAppBarConfiguration;
    private   DrawerLayout drawer;
    private NavController navController;
    SharedPreferencesHelper sharedPreferencesHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        niftyDialogBuilder = NiftyDialogBuilder.getInstance(this);
        sharedPreferencesHelper= new SharedPreferencesHelper(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail();

            }
        });

         drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_news, R.id.nav_programs,
                R.id.nav_wac_info, R.id.nav_contactus)
                .setDrawerLayout(drawer)
                .build();


        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_language) {
            dialog = new Dialog(this);

            dialog.setContentView(R.layout.activity_language);

            Button btnAr = (Button) dialog.findViewById(R.id.btn_ar);
            Button btnEn = (Button) dialog.findViewById(R.id.btn_en);


            btnAr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   // saveLocale("ar");
                    if (sharedPreferencesHelper.getLangCode().equals("en")) {
                        sharedPreferencesHelper.saveLanguage("ar");
//                    saveLocale("en");
                        Intent intent = new Intent(MainActivity.this,SplashActivity.class);
                        startActivity(intent);}
                    dialog.dismiss();
                }
            });

            btnEn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sharedPreferencesHelper.getLangCode().equals("ar")) {
                        sharedPreferencesHelper.saveLanguage("en");
//                    saveLocale("en");
                      Intent intent = new Intent(MainActivity.this,SplashActivity.class);
                      startActivity(intent);}
                   dialog.dismiss();
                }
            });
            dialog.show();


        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        if (menuItem.getItemId() == R.id.nav_share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "مركز شؤون المرأة"+"\n"+"com.wac.wacapplication";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "مركز شؤون المرأة");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
        else if (menuItem.getItemId() == R.id.nav_exit)
          finishAffinity();
        else if (menuItem.getItemId() == R.id.nav_home)
            navController.navigate(R.id.nav_home);
        else if (menuItem.getItemId() == R.id.nav_news)
            navController.navigate(R.id.nav_news);
        else if (menuItem.getItemId() == R.id.nav_programs)
            navController.navigate(R.id.nav_programs);
        else if (menuItem.getItemId() == R.id.nav_wac_info)
            navController.navigate(R.id.nav_wac_info);
        else if (menuItem.getItemId() == R.id.nav_contactus)
            navController.navigate(R.id.nav_contactus);
//        else if (menuItem.getItemId() == R.id.nav_contactus);
//            navController.navigate(R.id.nav_contactus);

        drawer.closeDrawers();
        return true;
    }
    //----------------------------------------------------------------------

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }


    protected void sendEmail() {


        String[] TO = {"info@wac.org.ps"};
//        String[] CC = {"xyz@gmail.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");


        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
     //   emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this,
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.removeItem(android.R.id.cut);

    }

}
